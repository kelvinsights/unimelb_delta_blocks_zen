<?php
/**
 * @file
 * unimelb_delta_blocks_zen.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function unimelb_delta_blocks_zen_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'replace_site_elements_with_delta_blocks_unimelb_zen';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~admin/*' => '~admin/*',
      ),
    ),
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'delta_blocks-site-name' => array(
          'module' => 'delta_blocks',
          'delta' => 'site-name',
          'region' => 'header',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;
  $export['replace_site_elements_with_delta_blocks_unimelb_zen'] = $context;

  return $export;
}
